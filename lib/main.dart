import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'dart:async';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: FadeThroughPageTransitionsBuilder(),
            TargetPlatform.iOS: FadeThroughPageTransitionsBuilder(),
          },
        ),
      ),
      routes: {
        '/' : (BuildContext context) {
          return Container(
            color: Color.fromRGBO(1, 51, 104, 1),
            //child:
            //  ContainerTransform(),
            child: Center(
              child: MaterialButton(
                child: Image.asset('images/NFL.png'),
                onPressed: () {
                  Navigator.of(context).pushNamed('/loading');
                },
              ),
            ),
          );
        },

        '/loading' : (BuildContext context) {
          Timer(Duration(seconds: 1), () {
            Navigator.of(context).pushNamed('/standings');
          });
          return MaterialButton(
            child: CircularProgressIndicator(),
          );
        },

        '/standings' : (BuildContext context) {
          return ListView (
            children: [
              Container(
                color: Color.fromRGBO(1, 51, 104, 1),
                //child:
                //  ContainerTransform(),
                child: Center(
                  child: MaterialButton(
                    child: Image.asset('images/NFL.png'),
                      onPressed: () {
                      Navigator.of(context).pushNamed('/');
                    },
                  ),
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Container(
                child: Text(
                  'NFL STANDINGS',
                  style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold, color: Color.fromRGBO(1, 51, 104, 1)),
                  textAlign: TextAlign.center,
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('AFC West'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcwest');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: AFCWestStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcwest');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('AFC East'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afceast');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: AFCEastStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afceast');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('AFC North'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcnorth');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: AFCNorthStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcnorth');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('AFC South'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcsouth');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: AFCSouthStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/afcsouth');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('NFC West'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcwest');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: NFCWestStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcwest');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('NFC East'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfceast');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: NFCEastStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfceast');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('NFC North'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcnorth');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: NFCNorthStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcnorth');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('NFC South'),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcsouth');
                  },
                ),
              ),
              Align(
                child: RaisedButton(
                  child: NFCSouthStandings(),
                  onPressed: () {
                    Navigator.of(context).pushNamed('/standings/nfcsouth');
                  },
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),

              //invisible spacing box
              SizedBox(height: 20),

            ],
          );
        },

        '/standings/afcwest' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: chiefsContainer(),
              ),
              Container(
                child: raidersContainer(),
              ),
              Container(
                child: broncosContainer(),
              ),
              Container(
                child: chargersContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],

          );
        },

        '/standings/afceast' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: billsContainer(),
              ),
              Container(
                child: dolphinsContainer(),
              ),
              Container(
                child: patriotsContainer(),
              ),
              Container(
                child: jetsContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/afcnorth' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: steelersContainer(),
              ),
              Container(
                child: ravensContainer(),
              ),
              Container(
                child: brownsContainer(),
              ),
              Container(
                child: bengalsContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/afcsouth' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: coltsContainer(),
              ),
              Container(
                child: titansContainer(),
              ),
              Container(
                child: texansContainer(),
              ),
              Container(
                child: jaguarsContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/nfcwest' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: cardinalsContainer(),
              ),
              Container(
                child: ramsContainer(),
              ),
              Container(
                child: seahawksContainer(),
              ),
              Container(
                child: fortyninersContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/nfceast' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: eaglesContainer(),
              ),
              Container(
                child: giantsContainer(),
              ),
              Container(
                child: teamContainer(),
              ),
              Container(
                child: cowboysContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/nfcnorth' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: packersContainer(),
              ),
              Container(
                child: bearsContainer(),
              ),
              Container(
                child: vikingsContainer(),
              ),
              Container(
                child: lionsContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

        '/standings/nfcsouth' : (BuildContext context) {
          return GridView.count(
            padding: const EdgeInsets.all(20),
            crossAxisSpacing: 10,
            mainAxisSpacing: 10,
            crossAxisCount: 2,
            childAspectRatio: 200 / 200,

            children: <Widget>[
              Container(
                child: saintsContainer(),
              ),
              Container(
                child: buccaneersContainer(),
              ),
              Container(
                child: falconsContainer(),
              ),
              Container(
                child: panthersContainer(),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Home'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pushNamed('/'),
                ),
              ),
              Align(
                child: RaisedButton(
                  child: Text('Back'),
                  //textColor: Colors.black,
                  //color: orange,
                  onPressed: () => Navigator.of(context).pop(),
                ),
              ),
            ],
          );
        },

      },
    );
  }
}

class ContainerTransform extends StatefulWidget {
  @override
  ContainerTransformState createState() => new ContainerTransformState();
}

class ContainerTransformState extends State<ContainerTransform> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => ExampleOpenedContainer(),
      closedBuilder: (BuildContext context, VoidCallback action) => ExampleClosedContainer(),
      tappable: true,
    );
  }
}

class ExampleOpenedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Chargers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Los Angeles Chargers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),


        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class ExampleClosedContainer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Chargers.png'),
    );
  }
}

class NFCWestStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
            'NFC WEST',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Arizona Cardinals',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Los Angeles Rams',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Seattle Seahawks',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'San Francisco 49ers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '4',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.400',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class NFCEastStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'NFC EAST',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Philadelphia Eagles',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.389',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'New York Giants',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.300',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Washington Football Team',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.222',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Dallas Cowboys',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.222',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class NFCSouthStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'NFC SOUTH',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'New Orleans Saints',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.778',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Tampa Bay Buccaneers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.700',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Atlanta Falcons',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.333',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Carolina Panthers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.300',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class NFCNorthStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'NFC NORTH',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Green Bay Packers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.778',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Chicago Bears',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.500',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Minnesota Vikings',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '4',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.444',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Detroit Lions',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '4',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.444',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class AFCEastStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'AFC EAST',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Buffalo Bills',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.700',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Miami Dolphins',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'New England Patriots',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '4',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '5',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.444',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'New York Jets',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '9',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.000',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class AFCSouthStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'AFC SOUTH',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Indianapolis Colts',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Tennessee Titans',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Houston Texans',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.222',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Jacksonville Jaguars',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '8',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.111',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class AFCNorthStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'AFC NORTH',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Pittsburgh Steelers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '9',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '1.000',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Baltimore Ravens',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Cleveland Browns',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Cincinnati Bengals',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.278',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class AFCWestStandings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DataTable(
      horizontalMargin: 8.0,
      columnSpacing: 8.0,
      headingRowHeight: 32.0,
      columns: const <DataColumn>[
        DataColumn(
          label: Expanded(
            child: Text(
              'AFC WEST',
              style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
            ),
          ),
        ),
        DataColumn(
          label: Text(
            'W',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'L',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'T',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
        DataColumn(
          label: Text(
            'PCT',
            style: TextStyle(fontSize: 16, fontStyle: FontStyle.italic, fontWeight: FontWeight.bold),
          ),
        ),
      ],
      rows: const <DataRow>[
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Kansas City Chiefs',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '8',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '1',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.889',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Las Vegas Raiders',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.667',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Denver Broncos',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '3',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '6',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.333',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
        DataRow(
          cells: <DataCell>[
            DataCell(
              Text(
                'Los Angeles Chargers',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '2',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '7',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            DataCell(
              Text(
                '0.222',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ],
        ),
      ],
    );
  }
}

class fortyninersContainer extends StatefulWidget {
  @override
  fortyninersState createState() => new fortyninersState();
}

class fortyninersState extends State<fortyninersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => fortyninersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => fortyninersClosed(),
      tappable: true,
    );
  }
}

class fortyninersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/49ers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'San Francisco 49ers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class fortyninersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/49ers.png'),
    );
  }
}

class bearsContainer extends StatefulWidget {
  @override
  bearsState createState() => new bearsState();
}

class bearsState extends State<bearsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => bearsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => bearsClosed(),
      tappable: true,
    );
  }
}

class bearsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Bears.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Chicago Bears',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class bearsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Bears.png'),
    );
  }
}

class bengalsContainer extends StatefulWidget {
  @override
  bengalsState createState() => new bengalsState();
}

class bengalsState extends State<bengalsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => bengalsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => bengalsClosed(),
      tappable: true,
    );
  }
}

class bengalsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Bengals.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Cincinnati Bengals',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class bengalsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Bengals.png'),
    );
  }
}

class billsContainer extends StatefulWidget {
  @override
  billsState createState() => new billsState();
}

class billsState extends State<billsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => billsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => billsClosed(),
      tappable: true,
    );
  }
}

class billsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Bills.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Buffalo Bills',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class billsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Bills.png'),
    );
  }
}

class broncosContainer extends StatefulWidget {
  @override
  broncosState createState() => new broncosState();
}

class broncosState extends State<broncosContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => broncosOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => broncosClosed(),
      tappable: true,
    );
  }
}

class broncosOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Broncos.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Denver Broncos',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class broncosClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Broncos.png'),
    );
  }
}

class brownsContainer extends StatefulWidget {
  @override
  brownsState createState() => new brownsState();
}

class brownsState extends State<brownsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => brownsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => brownsClosed(),
      tappable: true,
    );
  }
}

class brownsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Browns.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Cleveland Browns',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class brownsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Browns.png'),
    );
  }
}

class buccaneersContainer extends StatefulWidget {
  @override
  buccaneersState createState() => new buccaneersState();
}

class buccaneersState extends State<buccaneersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => buccaneersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => buccaneersClosed(),
      tappable: true,
    );
  }
}

class buccaneersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Buccaneers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Tampa Bay Buccaneers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class buccaneersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Buccaneers.png'),
    );
  }
}

class cardinalsContainer extends StatefulWidget {
  @override
  cardinalsState createState() => new cardinalsState();
}

class cardinalsState extends State<cardinalsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => cardinalsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => cardinalsClosed(),
      tappable: true,
    );
  }
}

class cardinalsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Cardinals.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Arizona Cardinals',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class cardinalsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Cardinals.png'),
    );
  }
}

class chargersContainer extends StatefulWidget {
  @override
  chargersState createState() => new chargersState();
}

class chargersState extends State<chargersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => chargersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => chargersClosed(),
      tappable: true,
    );
  }
}

class chargersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Chargers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Los Angeles Chargers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class chargersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Chargers.png'),
    );
  }
}

class chiefsContainer extends StatefulWidget {
  @override
  chiefsState createState() => new chiefsState();
}

class chiefsState extends State<chiefsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => chiefsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => chiefsClosed(),
      tappable: true,
    );
  }
}

class chiefsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Chiefs.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Kansas City Chiefs',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class chiefsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Chiefs.png'),
    );
  }
}

class coltsContainer extends StatefulWidget {
  @override
  coltsState createState() => new coltsState();
}

class coltsState extends State<coltsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => coltsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => coltsClosed(),
      tappable: true,
    );
  }
}

class coltsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Colts.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Indianapolis Colts',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class coltsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Colts.png'),
    );
  }
}

class cowboysContainer extends StatefulWidget {
  @override
  cowboysState createState() => new cowboysState();
}

class cowboysState extends State<cowboysContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => cowboysOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => cowboysClosed(),
      tappable: true,
    );
  }
}

class cowboysOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Cowboys.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Dallas Cowboys',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class cowboysClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Cowboys.png'),
    );
  }
}

class dolphinsContainer extends StatefulWidget {
  @override
  dolphinsState createState() => new dolphinsState();
}

class dolphinsState extends State<dolphinsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => dolphinsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => dolphinsClosed(),
      tappable: true,
    );
  }
}

class dolphinsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Dolphins.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Miami Dolphins',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class dolphinsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Dolphins.png'),
    );
  }
}

class eaglesContainer extends StatefulWidget {
  @override
  eaglesState createState() => new eaglesState();
}

class eaglesState extends State<eaglesContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => eaglesOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => eaglesClosed(),
      tappable: true,
    );
  }
}

class eaglesOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Eagles.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Philadelphia Eagles',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class eaglesClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Eagles.png'),
    );
  }
}

class falconsContainer extends StatefulWidget {
  @override
  falconsState createState() => new falconsState();
}

class falconsState extends State<falconsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => falconsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => falconsClosed(),
      tappable: true,
    );
  }
}

class falconsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Falcons.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Atlanta Falcons',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class falconsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Falcons.png'),
    );
  }
}

class giantsContainer extends StatefulWidget {
  @override
  giantsState createState() => new giantsState();
}

class giantsState extends State<giantsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => giantsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => giantsClosed(),
      tappable: true,
    );
  }
}

class giantsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Giants.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'New York Giants',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class giantsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Giants.png'),
    );
  }
}

class jaguarsContainer extends StatefulWidget {
  @override
  jaguarsState createState() => new jaguarsState();
}

class jaguarsState extends State<jaguarsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => jaguarsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => jaguarsClosed(),
      tappable: true,
    );
  }
}

class jaguarsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Jaguars.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Jacksonville Jaguars',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class jaguarsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Jaguars.png'),
    );
  }
}

class jetsContainer extends StatefulWidget {
  @override
  jetsState createState() => new jetsState();
}

class jetsState extends State<jetsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => jetsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => jetsClosed(),
      tappable: true,
    );
  }
}

class jetsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Jets.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'New York Jets',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class jetsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Jets.png'),
    );
  }
}

class lionsContainer extends StatefulWidget {
  @override
  lionsState createState() => new lionsState();
}

class lionsState extends State<lionsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => lionsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => lionsClosed(),
      tappable: true,
    );
  }
}

class lionsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Lions.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Detroit Lions',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class lionsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Lions.png'),
    );
  }
}

class packersContainer extends StatefulWidget {
  @override
  packersState createState() => new packersState();
}

class packersState extends State<packersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => packersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => packersClosed(),
      tappable: true,
    );
  }
}

class packersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Packers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Green Bay Packers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class packersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Packers.png'),
    );
  }
}

class panthersContainer extends StatefulWidget {
  @override
  panthersState createState() => new panthersState();
}

class panthersState extends State<panthersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => panthersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => panthersClosed(),
      tappable: true,
    );
  }
}

class panthersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Panthers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Carolina Panthers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class panthersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Panthers.png'),
    );
  }
}

class patriotsContainer extends StatefulWidget {
  @override
  patriotsState createState() => new patriotsState();
}

class patriotsState extends State<patriotsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => patriotsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => patriotsClosed(),
      tappable: true,
    );
  }
}

class patriotsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Patriots.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'New England Patriots',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class patriotsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Patriots.png'),
    );
  }
}

class raidersContainer extends StatefulWidget {
  @override
  raidersState createState() => new raidersState();
}

class raidersState extends State<raidersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => raidersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => raidersClosed(),
      tappable: true,
    );
  }
}

class raidersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Raiders.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Las Vegas Raiders',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class raidersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Raiders.png'),
    );
  }
}

class ramsContainer extends StatefulWidget {
  @override
  ramsState createState() => new ramsState();
}

class ramsState extends State<ramsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => ramsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => ramsClosed(),
      tappable: true,
    );
  }
}

class ramsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Rams.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Los Angeles Rams',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class ramsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Rams.png'),
    );
  }
}

class ravensContainer extends StatefulWidget {
  @override
  ravensState createState() => new ravensState();
}

class ravensState extends State<ravensContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => ravensOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => ravensClosed(),
      tappable: true,
    );
  }
}

class ravensOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Ravens.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Baltimore Ravens',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class ravensClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Ravens.png'),
    );
  }
}

class saintsContainer extends StatefulWidget {
  @override
  saintsState createState() => new saintsState();
}

class saintsState extends State<saintsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => saintsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => saintsClosed(),
      tappable: true,
    );
  }
}

class saintsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Saints.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'New Orleans Saints',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class saintsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Saints.png'),
    );
  }
}

class seahawksContainer extends StatefulWidget {
  @override
  seahawksState createState() => new seahawksState();
}

class seahawksState extends State<seahawksContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => seahawksOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => seahawksClosed(),
      tappable: true,
    );
  }
}

class seahawksOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Seahawks.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Seattle Seahawks',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class seahawksClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Seahawks.png'),
    );
  }
}

class steelersContainer extends StatefulWidget {
  @override
  steelersState createState() => new steelersState();
}

class steelersState extends State<steelersContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => steelersOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => steelersClosed(),
      tappable: true,
    );
  }
}

class steelersOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Steelers.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Pittsburgh Steelers',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class steelersClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Steelers.png'),
    );
  }
}

class teamContainer extends StatefulWidget {
  @override
  teamState createState() => new teamState();
}

class teamState extends State<teamContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => teamOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => teamClosed(),
      tappable: true,
    );
  }
}

class teamOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Team.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Washington Football Team',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class teamClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Team.png'),
    );
  }
}

class texansContainer extends StatefulWidget {
  @override
  texansState createState() => new texansState();
}

class texansState extends State<texansContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => texansOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => texansClosed(),
      tappable: true,
    );
  }
}

class texansOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Texans.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Houston Texans',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class texansClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Texans.png'),
    );
  }
}

class titansContainer extends StatefulWidget {
  @override
  titansState createState() => new titansState();
}

class titansState extends State<titansContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => titansOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => titansClosed(),
      tappable: true,
    );
  }
}

class titansOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Titans.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Tennessee Titans',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class titansClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Titans.png'),
    );
  }
}

class vikingsContainer extends StatefulWidget {
  @override
  vikingsState createState() => new vikingsState();
}

class vikingsState extends State<vikingsContainer> {
  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      openBuilder: (BuildContext context, VoidCallback action) => vikingsOpened(),
      closedBuilder: (BuildContext context, VoidCallback action) => vikingsClosed(),
      tappable: true,
    );
  }
}

class vikingsOpened extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Image.asset(
          'images/Vikings.png',
          fit: BoxFit.cover,
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Container(
          child: Text(
            'Minnesota Vikings',
            style: TextStyle(fontSize: 32, fontWeight: FontWeight.bold),
            textAlign: TextAlign.center,
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

        Align(
          child: RaisedButton(
            child: Text('Close'),
            //textColor: Colors.black,
            //color: orange,
            onPressed: () => Navigator.of(context).pop(),
          ),
        ),

        //invisible spacing box
        SizedBox(height: 20),

      ],
    );
  }
}

class vikingsClosed extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Image.asset('images/Vikings.png'),
    );
  }
}